﻿using System;
using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class FieldWriterTest
    {
        [Test]
        public void WriteValueTest()
        {
            Assert.Throws<ArgumentNullException>(() =>
                {
                    var fieldWriter = new FieldWriter<FieldTestClass, string>(null, "asdf");
                    Assert.NotNull(fieldWriter);
                },
                "There should be an ArgumentNullException because of the first argument which is null!");
            Assert.Throws<ArgumentNullException>(() =>
                {
                    var fieldWriter = new FieldWriter<FieldTestClass, string>(typeof(FieldTestClass), (string)null);
                    Assert.NotNull(fieldWriter);
                },
                "There should be an ArgumentNullException because of the second argument which is null!");

            FieldWriter<FieldTestClass, int> fieldReader =
                new FieldWriter<FieldTestClass, int>(typeof(FieldTestClass), "PublicIntegerValue");

            FieldTestClass f = new FieldTestClass();
            {
                const int v = 5;
                fieldReader.SetValue(ref f, v);
                Assert.AreEqual(v, f.PublicIntegerValue, "Wrong value!");
                var o = (object) v;
                object f1 = f;
                fieldReader.SetUntypedValue(ref f1, o);
                Assert.AreEqual(o, f.PublicIntegerValue, "Wrong object value!");
            }
            {
                const int v = 4;
                fieldReader.SetValue(ref f, v);
                Assert.AreEqual(v, f.PublicIntegerValue, "Wrong new value!");
                var o = (object) v;
                object f1 = f;
                fieldReader.SetUntypedValue(ref f1, o);
                Assert.AreEqual(o, f.PublicIntegerValue, "Wrong new object value!");
            }
        }

        
        [Test]
        public void WriteStaticValueTest()
        {
            FieldWriter<WithStaticField, string> fieldReader = new FieldWriter<WithStaticField, string>(typeof(WithStaticField), "Name");

            WithStaticField f = new WithStaticField();
            {
                const string v = "Hello";
                fieldReader.SetValue(ref f, v);
                Assert.AreEqual(v, WithStaticField.Name, "Wrong value!");
                var o = (object)v;
                object f1 = f;
                fieldReader.SetUntypedValue(ref f1, o);
                Assert.AreEqual(o, WithStaticField.Name, "Wrong object value!");
            }
            {
                const string v = "World";
                fieldReader.SetValue(ref f, v);
                Assert.AreEqual(v, WithStaticField.Name, "Wrong new value!");
                var o = (object)v;
                object f1 = f;
                fieldReader.SetUntypedValue(ref f1, o);
                Assert.AreEqual(o, WithStaticField.Name, "Wrong new object value!");
            }
        }
    }
}
