﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class MemberKeyTest
    {
        [Test]
        public void EqualityTest()
        {
            MemberKey memberKey0 = new MemberKey(typeof(FieldExample2), "modificationDate");
            MemberKey memberKey1 = new MemberKey(typeof(FieldExample2), "modificationDate");
            Assert.AreEqual(memberKey0, memberKey1, "key 0 and 1 should be equal.");
            MemberKey memberKey2 = new MemberKey(typeof(FieldExample2), "lifeTime");
            Assert.AreNotEqual(memberKey0, memberKey2, "key 0 and 2 should not be equal");
            Assert.AreNotEqual(memberKey1, memberKey2, "key 1 and 2 should not be equal");
            Assert.AreNotEqual(null, memberKey0, "Null and key 0 should not be equal");
            Assert.AreNotEqual(null, memberKey1, "Null and key 1 should not be equal");
            Assert.AreNotEqual(null, memberKey2, "Null and key 2 should not be equal");
            Assert.AreNotEqual(memberKey0, null, "key 0 and Null should not be equal");
            Assert.AreNotEqual(memberKey1, null, "key 1 and Null should not be equal");
            Assert.AreNotEqual(memberKey2, null, "key 2 and Null should not be equal");
            Assert.AreEqual(null, null, "Null and Null should not be equal");

            HashSet<MemberKey> hashSet = new HashSet<MemberKey>();
            Assert.AreEqual(0, hashSet.Count, "Wrong init!");
            hashSet.Add(memberKey0);
            Assert.AreEqual(1, hashSet.Count, "Key 0 added!");
            hashSet.Add(memberKey1);
            Assert.AreEqual(1, hashSet.Count, "Key 1 added, should be same as 0 so still count 1!");
            hashSet.Add(memberKey2);
            Assert.AreEqual(2, hashSet.Count, "Key 2 added, so we should have totally two different!");

            Assert.IsTrue(MemberKey.Equals(memberKey0, memberKey1), "Key 0 and key 1 should be equal.");
            Assert.IsFalse(MemberKey.Equals(memberKey0, memberKey2), "Key 0 and key 2 should not be equal.");
            Assert.IsFalse(MemberKey.Equals(null, memberKey2), "Null and key 2 should not be equal.");
            Assert.IsFalse(MemberKey.Equals(memberKey2, null), "Key 2 and null should not be equal.");
            Assert.IsTrue(MemberKey.Equals(null, null), "Null and null should not be equal.");

            {
                int code0 = memberKey0.GetHashCode();
                int code1 = memberKey1.GetHashCode();
                int code2 = memberKey2.GetHashCode();
                Assert.AreEqual(code0, code1, "0 and 1 should be equal!");
                Assert.AreEqual(code1, code0, "1 and 0 should be equal!");
                Assert.AreNotEqual(code0, code2, "0 and 2 should not be equal!");
                Assert.AreNotEqual(code1, code2, "1 and 2 should not be equal!");
            }

            {
                IEqualityComparer<MemberKey> equalityComparer = memberKey0;
                int code0 = equalityComparer.GetHashCode(memberKey0);
                int code1 = equalityComparer.GetHashCode(memberKey1);
                int code2 = equalityComparer.GetHashCode(memberKey2);
                Assert.AreEqual(code0, code1, "0 and 1 should be equal!");
                Assert.AreEqual(code1, code0, "1 and 0 should be equal!");
                Assert.AreNotEqual(code0, code2, "0 and 2 should not be equal!");
                Assert.AreNotEqual(code1, code2, "1 and 2 should not be equal!");
            }

            {
                IEqualityComparer<MemberKey> equalityComparer = memberKey0;
                Assert.IsTrue(equalityComparer.Equals(memberKey0, memberKey1), "0 and 1 should be equal!");
                Assert.IsFalse(equalityComparer.Equals(memberKey0, memberKey2), "0 and 2 should not be equal!");
                Assert.IsFalse(equalityComparer.Equals(null, memberKey2), "null and 2 should not be equal!");
                Assert.IsFalse(equalityComparer.Equals(memberKey2, null), "2 and null should not be equal!");
                Assert.IsTrue(equalityComparer.Equals(null, null), "null and null should be equal!");
            }

        }

        [Test]
        public void NullArgumentTest()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                MemberKey memberKey3 = new MemberKey(typeof(object), null);
                Assert.NotNull(memberKey3);
            });
            
            Assert.Throws<ArgumentNullException>(() =>
            {
                MemberKey memberKey3 = new MemberKey(null, "myField");
                Assert.NotNull(memberKey3);
            });        
        }

        [Test]
        public void ArguementEmptyStringTest()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                MemberKey memberKey3 = new MemberKey(typeof(object), "");
                Assert.NotNull(memberKey3);
            });
        }
    }
}
