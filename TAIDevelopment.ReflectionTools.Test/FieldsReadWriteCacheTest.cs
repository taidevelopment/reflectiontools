﻿using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class FieldsReadWriteCacheTest
    {
        [Test]
        public void PublicIntegerFieldReadTest()
        {
            FieldTestClass obj = new FieldTestClass();
            IFieldsReadWriteCache fieldsReadWriteCache = new FieldsReadWriteCache();
            obj.PublicIntegerValue = 42;
            object objC = obj;
            Assert.AreEqual(obj.PublicIntegerValue, fieldsReadWriteCache.Get(ref objC, nameof(obj.PublicIntegerValue)));
            obj.PublicIntegerValue = 13;
            Assert.AreEqual(obj.PublicIntegerValue, fieldsReadWriteCache.Get(ref objC, nameof(obj.PublicIntegerValue)));
            
            obj.PublicIntegerValue = 57;
            int typeValue = fieldsReadWriteCache.Get<int>(ref objC, nameof(obj.PublicIntegerValue));
            Assert.AreEqual(obj.PublicIntegerValue, fieldsReadWriteCache.Get(ref objC, nameof(obj.PublicIntegerValue)));

            (fieldsReadWriteCache as FieldsReadWriteCache)?.Clear();
            Assert.AreEqual(0, (fieldsReadWriteCache as FieldsReadWriteCache).ReadersCount(), "There should be no readers after 'clear'.");
            Assert.AreEqual(0, (fieldsReadWriteCache as FieldsReadWriteCache).WritersCount(), "There should be no writers after 'clear'.");
        }
        
        [Test]
        public void PublicIntegerFieldWriteTest()
        {
            FieldTestClass obj = new FieldTestClass();
            IFieldsReadWriteCache fieldsReadWriteCache = new FieldsReadWriteCache();
            object objC = obj;
            fieldsReadWriteCache.Set(ref objC, nameof(obj.PublicIntegerValue), 42);
            Assert.AreEqual(42, obj.PublicIntegerValue);
            fieldsReadWriteCache.Set(ref objC, nameof(obj.PublicIntegerValue), 13);
            Assert.AreEqual(13, obj.PublicIntegerValue);

            (fieldsReadWriteCache as FieldsReadWriteCache)?.Clear();
            Assert.AreEqual(0, (fieldsReadWriteCache as FieldsReadWriteCache).ReadersCount(), "There should be no readers after 'clear'.");
            Assert.AreEqual(0, (fieldsReadWriteCache as FieldsReadWriteCache).WritersCount(), "There should be no writers after 'clear'.");
        }

        [Test]
        public void PublicStaticIntegerFieldWriteTest()
        {
            FieldTestClass obj = new FieldTestClass();
            IFieldsReadWriteCache fieldsReadWriteCache = new FieldsReadWriteCache();
            object objC = obj;
            fieldsReadWriteCache.Set(ref objC, nameof(FieldTestClass.PublicStaticValue), 42);
            Assert.AreEqual(42, FieldTestClass.PublicStaticValue);
            fieldsReadWriteCache.Set(ref objC, nameof(FieldTestClass.PublicStaticValue), 13);
            Assert.AreEqual(13, FieldTestClass.PublicStaticValue);

        }

        [Test]
        public void ShouldExecutePostWriteActionIfAttributeIndicates()
        {
            FieldsTestClassWithAttributes obj = new FieldsTestClassWithAttributes();
            IFieldsReadWriteCache fieldsReadWriteCache = new FieldsReadWriteCache();
            
            Assert.False(obj.PostWriteLogicCalled);
            fieldsReadWriteCache.Set(ref obj, nameof(obj.Integer), 42);
            Assert.AreEqual(42, obj.Integer);
            Assert.True(obj.PostWriteLogicCalled);
        }
    }
}