using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class TypeNameWriterTest
    {
        [Test]
        public void GetTypeNameForJsonTest()
        {
            Type testType = typeof(TypeNameTestClass);
            string typeName = TypeNameWriter.GetTypeNameForJson(testType);
            Type testType2 = TypeGenerator.GetType(typeName);
            Assert.AreEqual(testType, testType2);
        }

        [Explicit("WIP")]
        [Test]
        public void GetTypeNameForJsonStringTest()
        {
            string typeName = TypeNameWriter.GetTypeNameForJson(typeof(TypeNameTestClass));
            Assert.AreEqual("Sjoerd222888.ReflectionTools.Test.TestCases.TypeNameTestClass, Sjoerd222888.ReflectionTools.Test", typeName);
        }
        
        [Test]
        public void GetTypeNameForJsonSubClassTest()
        {
            Type testType = typeof(TypeNameTestClass.SubClass);
            string typeName = TypeNameWriter.GetTypeNameForJson(testType);
            Type testType2 = TypeGenerator.GetType(typeName);
            Assert.AreEqual(testType, testType2);
        }
        
        [Explicit("WIP")]
        [Test]
        public void GetTypeNameForJsonSubClassStringTest()
        {
            string typeName = TypeNameWriter.GetTypeNameForJson(typeof(TypeNameTestClass.SubClass));
            Assert.AreEqual("Sjoerd222888.ReflectionTools.Test.TestCases.TypeNameTestClass+SubClass, Sjoerd222888.ReflectionTools.Test", typeName);
        }

        [Test]
        public void PropertyChangedArgumentTypeTest()
        {
            Type testType = typeof(PropertyChangedEventArgs);
            string typeName = TypeNameWriter.GetTypeNameForJson(testType);
            Type testType2 = TypeGenerator.GetType(typeName);
            Assert.AreEqual(testType, testType2);
        }
        
        [Explicit("WIP")]
        [Test]
        public void PropertyChangedArgumentTypeStringTest()
        {
            string typeName = TypeNameWriter.GetTypeNameForJson(typeof(PropertyChangedEventArgs));
            Assert.AreEqual("System.ComponentModel.PropertyChangedEventArgs, System.ObjectModel", typeName);
        }

        [Test]
        public void ComplexGenericType1Test()
        {
            Type testType = typeof(List<ConcurrentDictionary<string, Tuple<int, string, char, double[][], byte, short, long[], Type>>>);
            string typeName = TypeNameWriter.GetTypeNameForJson(testType);
            Type testType2 = TypeGenerator.GetType(typeName);
            Assert.AreEqual(testType, testType2);
        }
        
        [Explicit("WIP")]
        [Test]
        public void ComplexGenericType1StringTest()
        {
            Type testType = typeof(List<ConcurrentDictionary<string, Tuple<int, string, char, double[][], byte, short, long[], Type>>>);
            string typeName = TypeNameWriter.GetTypeNameForJson(testType);
            Assert.AreEqual("System.Collections.Generic.List`1[[System.Collections.Concurrent.ConcurrentDictionary`2[[System.String, System.Private.CoreLib],[System.Tuple`8[[System.Int32, System.Private.CoreLib],[System.String, System.Private.CoreLib],[System.Char, System.Private.CoreLib],[System.Double[][], System.Private.CoreLib],[System.Byte, System.Private.CoreLib],[System.Int16, System.Private.CoreLib],[System.Int64[], System.Private.CoreLib],[System.Type, System.Private.CoreLib]], System.Private.CoreLib]], System.Collections.Concurrent]], System.Private.CoreLib", typeName);
        }
    }
}