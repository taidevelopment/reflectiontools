using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using NUnit.Framework;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class TypeGeneratorTest
    {
        [Test]
        public void ComplicatedConcurrentDictionaryTypeTest()
        {
            string typeName =
                TypeNameWriter.GetTypeNameForJson(
                    typeof(ConcurrentDictionary<INotifyPropertyChanged, PropertyChangedEventHandler>));

            Type type = TypeGenerator.GetType(typeName);
            Assert.NotNull(type);
        }
        
        [Test]
        public void Test1()
        {
            string typeName =
                TypeNameWriter.GetTypeNameForJson(typeof(INotifyPropertyChanged));

            Type type = TypeGenerator.GetType(typeName);
            Assert.NotNull(type);
        }
        
        [Test]
        public void Test2()
        {
            string typeName =
                TypeNameWriter.GetTypeNameForJson(typeof(PropertyChangedEventHandler));

            Type type = TypeGenerator.GetType(typeName);
            Assert.NotNull(type);
        }
        
    }
}