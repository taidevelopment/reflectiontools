using System.Reflection;
using System.Threading.Tasks;
using NUnit.Framework;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class MethodInfoCacheTest
    {
        [SetUp]
        public void OneTimeSetup()
        {
            MethodInfoCache.Clear();
        }
        
        
        [Test]
        public void MultiSignatureTest()
        {
            MethodInfo methodInfo = MethodInfoCache.GetMethodInfo(
                typeof(MethodInfoCacheTest),
                nameof(Meth1),
                typeof(int));
            int result = (int)methodInfo.Invoke(this, new object[] {1});
            Assert.AreEqual(1, result);
            
            methodInfo = MethodInfoCache.GetMethodInfo(
                typeof(MethodInfoCacheTest),
                nameof(Meth1),
                typeof(int), typeof(string));
            result = (int)methodInfo.Invoke(this, new object[] {1, "s"});
            Assert.AreEqual(2, result);
        }

        public int Meth1(int i)
        {
            return 1;
        }

        public int Meth1(int i, string s)
        {
            return 2;
        }

        [Test]
        public void TestCacheEntryUnique()
        {
            Assert.AreEqual(0, MethodInfoCache.Count());
            MethodInfo methodInfo = MethodInfoCache.GetMethodInfo(
                typeof(MethodInfoCacheTest),
                nameof(Meth1),
                typeof(int));
            Assert.AreEqual(1, MethodInfoCache.Count());
            methodInfo = MethodInfoCache.GetMethodInfo(
                typeof(MethodInfoCacheTest),
                nameof(Meth1),
                typeof(int));
            Assert.AreEqual(1, MethodInfoCache.Count());
        }

        private class BaseClass
        {
            public int MethOnBase(int a, int b)
            {
                return a + b;
            }
        }

        private class DerivedClass : BaseClass
        {
            
        }

        [Test]
        public void BaseClassMethodTest()
        {
            MethodInfo methodInfo = MethodInfoCache.GetMethodInfo(
                typeof(BaseClass),
                nameof(BaseClass.MethOnBase),
                typeof(int), typeof(int));
            
            Assert.NotNull(methodInfo);
            
            methodInfo = MethodInfoCache.GetMethodInfo(
                typeof(DerivedClass),
                nameof(BaseClass.MethOnBase),
                typeof(int), typeof(int));
            
            Assert.NotNull(methodInfo);
        }

        private class ClassWithAsyncMethod
        {
            private async Task<int> GetNumberAsync(int a, int b)
            {
                await Task.Delay(1);
                return a + b;
            }
        }
        
        private class ClassWithAsyncMethodDerived : ClassWithAsyncMethod
        {
           
        }

        [Test]
        public void AsyncMethodTest()
        {
            MethodInfo methodInfo = MethodInfoCache.GetMethodInfo(
                typeof(ClassWithAsyncMethod),
                "GetNumberAsync",
                typeof(int), typeof(int));
            
            Assert.NotNull(methodInfo);
            
            methodInfo = MethodInfoCache.GetMethodInfo(
                typeof(ClassWithAsyncMethodDerived),
                "GetNumberAsync",
                typeof(int), typeof(int));
            
            Assert.NotNull(methodInfo);
        }

    }
}