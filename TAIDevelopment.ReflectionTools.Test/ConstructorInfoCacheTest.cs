﻿using System;
using System.Reflection;
using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class ConstructorInfoCacheTest
    {
        [Test]
        public void GetParameterLessConstructorInfoTest()
        {
            Type type = typeof(ConstructorExample1);
            ConstructorInfo constructorInfo = ConstructorInfoCache.GetParameterLessConstructorInfo(type);
            Assert.IsNotNull(constructorInfo);
            Assert.AreEqual(type, constructorInfo.ReflectedType, "Wrong type.");
        }

        [Test]
        public void GetConstructorTest()
        {
            Type type = typeof(ConstructorExample1);
            {
                ConstructorInfo constructorInfo = ConstructorInfoCache.GetConstructor(type, new[] { typeof(string) });
                Assert.IsNotNull(constructorInfo);
                Assert.AreEqual(type, constructorInfo.ReflectedType, "Wrong type.");
            }
            {
                ConstructorInfo constructorInfo = ConstructorInfoCache.GetConstructor(type, new[] { typeof(int) });
                Assert.IsNotNull(constructorInfo);
                Assert.AreEqual(type, constructorInfo.ReflectedType, "Wrong type.");
            }
            {
                ConstructorInfo constructorInfo = ConstructorInfoCache.GetConstructor(type, new[] { typeof(string), typeof(int) });
                Assert.IsNotNull(constructorInfo);
                Assert.AreEqual(type, constructorInfo.ReflectedType, "Wrong type.");
            }
        }
    }
}
