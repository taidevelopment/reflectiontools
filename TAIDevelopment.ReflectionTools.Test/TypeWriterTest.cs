﻿using System;
using System.Diagnostics;
using NUnit.Framework;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class TypeWriterTest
    {
        [Test]
        public void TestString()
        {
            {
                const string str = "asdf";
                string s = TypeWriter.ToString(str);
                Assert.AreEqual("\"asdf\"", s);
            }
            {
                const string str = "";
                string s = TypeWriter.ToString(str);
                Assert.AreEqual("\"\"", s);
            }
            {
                const string str = null;
                string s = TypeWriter.ToString(str);
                Assert.AreEqual("null", s);
            }
        }

        [Test]
        public void TestLong()
        {
            const long l = 9223372036854775807L;
            string s = TypeWriter.ToString(l);
            Assert.AreEqual("9223372036854775807", s);
        }

        [Test]
        public void TestInt()
        {
            const int i = -2147483648;
            string s = TypeWriter.ToString(i);
            Assert.AreEqual("-2147483648", s);
        }

        [Test]
        public void TestShort()
        {
            const short sh = 32767;
            string s = TypeWriter.ToString(sh);
            Assert.AreEqual("32767", s);
        }

        [Test]
        public void TestByte()
        {
            const byte b = 255;
            string s = TypeWriter.ToString(b);
            Assert.AreEqual("255", s);
        }

        [Test]
        public void TestULong()
        {
            const ulong ul = 18446744073709551615UL;
            string s = TypeWriter.ToString(ul);
            Assert.AreEqual("18446744073709551615", s);
        }

        [Test]
        public void TestUInt()
        {
            const uint ui = 4294967295;
            string s = TypeWriter.ToString(ui);
            Assert.AreEqual("4294967295", s);
        }

        [Test]
        public void TestUShort()
        {
            const ushort us = 65535;
            string s = TypeWriter.ToString(us);
            Assert.AreEqual("65535", s);
        }

        [Test]
        public void TestDoubleRoundTrip()
        {
            const double d = Math.PI;
            string s = TypeWriter.ToString(d);
            double parsed = TypeParser.Parse<double>(s);
            Assert.AreEqual(parsed, d);
        }

        [Test]
        public void TestFloatRoundTrip()
        {
            const float f = float.MinValue;
            string s = TypeWriter.ToString(f);
            float parsed = TypeParser.Parse<float>(s);
            Assert.AreEqual(parsed, f);
        }

        [Test]
        public void TestDecimalRoundTrip()
        {
            const decimal m = decimal.MaxValue;
            string s = TypeWriter.ToString(m);
            decimal parsed = TypeParser.Parse<decimal>(s);
            Assert.AreEqual(parsed, m);
        }



        [Test]
        public void TestBool()
        {
            {
                const bool b = true;
                string s = TypeWriter.ToString(b);
                Assert.AreEqual("true", s);
            }
            {
                const bool b = false;
                string s = TypeWriter.ToString(b);
                Assert.AreEqual("false", s);
            }
        }

        [Test]
        public void TestNullableBool()
        {
            {
                bool? b = true;
                string s = TypeWriter.ToString(b);
                Assert.AreEqual("true", s);
            }
            {
                bool? b = null;
                string s = TypeWriter.ToString(b);
                Assert.AreEqual("null", s);
            }
        }

        [Test]
        public void TestChar()
        {
            const char c = 'Z';
            string s = TypeWriter.ToString(c);
            Assert.AreEqual("Z", s);
        }

        [Test]
        public void TestNullableChar()
        {
            {
                char? c = 'Z';
                string s = TypeWriter.ToString(c);
                Assert.AreEqual("Z", s);
            }
            {
                char? c = null;
                // ReSharper disable once ExpressionIsAlwaysNull
                string s = TypeWriter.ToString(c);
                Assert.AreEqual("null", s);
            }
        }

        [Test]
        public void TestSystemType()
        {
            Type type = typeof(Stopwatch);
            string s = TypeWriter.ToString(type);
            const string expected  = "\"System.Diagnostics.Stopwatch, System";
            Console.WriteLine(s);
            Assert.IsTrue(s.StartsWith(expected), "Wrong string: '{0}'", s);
        }


    }
}
