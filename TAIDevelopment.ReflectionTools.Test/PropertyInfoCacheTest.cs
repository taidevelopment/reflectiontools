﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class PropertyInfoCacheTest
    {
        [Test]
        public void GetPropertyInfoTest()
        {
            Type type = typeof(ConstructorExample1);
            {
                PropertyInfo propertyInfo = PropertyInfoCache.GetPropertyInfo(type, nameof(ConstructorExample1.S));
                Assert.IsNotNull(propertyInfo, "propertyInfo is null.");
                Assert.AreEqual(nameof(ConstructorExample1.S), propertyInfo.Name, "Wrong property name.");
            }
            {
                PropertyInfo propertyInfo = PropertyInfoCache.GetPropertyInfo(type, nameof(ConstructorExample1.I));
                Assert.IsNotNull(propertyInfo, "propertyInfo is null.");
                Assert.AreEqual(nameof(ConstructorExample1.I), propertyInfo.Name, "Wrong property name.");
            }
        }
        [Test]
        public void GetPropertyInfosTest()
        {
            Type type = typeof(ConstructorExample1);
            PropertyInfo[] propertyInfos = PropertyInfoCache.GetPropertyInfos(type);
            Assert.IsNotNull(propertyInfos, "propertyInfo is null.");
            Assert.AreEqual(2, propertyInfos.Length, "wrong number of propert infos.");
            PropertyInfo propertyInfo1 = propertyInfos.FirstOrDefault(i => i.Name == nameof(ConstructorExample1.S));
            Assert.IsNotNull(propertyInfo1, "Could not find property 'S'.");
            PropertyInfo propertyInfo2 = propertyInfos.FirstOrDefault(i => i.Name == nameof(ConstructorExample1.I));
            Assert.IsNotNull(propertyInfo2, "Could not find property 'I'.");
        }

    }
}
