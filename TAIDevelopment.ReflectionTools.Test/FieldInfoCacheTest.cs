using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class FieldInfoCacheTest
    {
        ConcurrentQueue<FieldInfo> _fieldInfoQueue = new ConcurrentQueue<FieldInfo>();
        ConcurrentQueue<FieldInfo[]> _fieldInfosQueue = new ConcurrentQueue<FieldInfo[]>();
        private const int Total = 3000;

        [Test]
        public async Task ParallelGetFieldInfoTest()
        {
            const string fieldName1 = "_privateIntegerValue";

            FieldInfoCache.Clear();
            Assert.AreEqual(0, FieldInfoCache.FieldInfoCount(), "Wrong count after clear!");

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < Total; ++i)
            {
                tasks.Add(GetFieldInfoAsync(i, typeof(FieldTestClass), fieldName1));
            }

            await Task.WhenAll(tasks).ConfigureAwait(false);

            Assert.AreEqual(Total, _fieldInfoQueue.Count, "Wrong number of field infos!");
            FieldInfo[] list = _fieldInfoQueue.ToArray();
            for (int i = 0; i < list.Length; i++)
            {
                FieldInfo f = list[i];
                Assert.IsNotNull(f, "Field is null for item '{0}'!", i);
                Assert.AreEqual(fieldName1, f.Name, "Wrong name!");
            }

            Assert.AreEqual(1, FieldInfoCache.FieldInfoCount(), "Wrong field count after test!");
        }

        [Explicit("I don't understand the interface implementation.")]
        [Test]
        public async Task ParallelGetFieldInfoForInterfaceTest()
        {
            string fieldName1 = "modificationDate";

            FieldInfoCache.Clear();
            Assert.AreEqual(0, FieldInfoCache.FieldInfoCount(), "Wrong count after clear!");

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < Total; ++i)
            {
                tasks.Add(GetFieldInfoAsync(i, typeof(IFieldExample2), fieldName1));
            }

            await Task.WhenAll(tasks).ConfigureAwait(false);

            Assert.AreEqual(Total, _fieldInfoQueue.Count, "Wrong number of field infos!");
            FieldInfo[] list = _fieldInfoQueue.ToArray();
            for (int i = 0; i < list.Length; i++)
            {
                FieldInfo f = list[i];
                Assert.IsNotNull(f, "Field is null for item '{0}'!", i);
                Assert.AreEqual(fieldName1, f.Name, "Wrong name!");
            }

            Assert.AreEqual(1, FieldInfoCache.FieldInfoCount(), "Wrong field count after test!");
        }

        [Test]
        public async Task ParallelGetFieldInfosTest()
        {
            const string fieldName1 = "_privateIntegerValue";
            const string propertyName1 = "PublicIntegerValue";

            FieldInfoCache.Clear();
            Assert.AreEqual(0, FieldInfoCache.FieldInfoArrayCount(), "Wrong count after clear!");

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < Total; ++i)
            {
                tasks.Add(GetFieldInfosAsync(i, typeof(FieldTestClass)));
            }

            await Task.WhenAll(tasks).ConfigureAwait(false);

            string[] names = {fieldName1, propertyName1};
            Assert.AreEqual(Total, _fieldInfosQueue.Count, "Wrong number of field infos!");
            FieldInfo[][] lists = _fieldInfosQueue.ToArray();
            for (int i = 0; i < lists.Length; ++i)
            {
                Assert.AreEqual(names.Length, lists[i].Length, "Wrong number of field infos!");
                for (int j = 0; j < lists[i].Length; ++j)
                {
                    FieldInfo f = lists[i][j];
                    Assert.IsNotNull(f, "Field is null for item '{0}.{1}'!", i, j);
                    Assert.IsNotNull(names.FirstOrDefault(l => l == f.Name), "Wrong name!");
                }
            }

            Assert.AreEqual(1, FieldInfoCache.FieldInfoArrayCount(), "Wrong array field count after test!");
        }

        [Explicit("I don't understand the interface implementation.")]
        [Test]
        public async Task ParallelGetFieldInfosForInterfaceTest()
        {
            string fieldName1 = "modificationDate";
            string fieldName2 = "lifeTime";

            FieldInfoCache.Clear();
            Assert.AreEqual(0, FieldInfoCache.FieldInfoArrayCount(), "Wrong count after clear!");

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < Total; ++i)
            {
                tasks.Add(GetFieldInfosAsync(i, typeof(IFieldExample2)));
            }

            await Task.WhenAll(tasks).ConfigureAwait(false);

            string[] names = {fieldName1, fieldName2};
            Assert.AreEqual(Total, _fieldInfosQueue.Count, "Wrong number of field infos!");
            FieldInfo[][] lists = _fieldInfosQueue.ToArray();
            for (int i = 0; i < lists.Length; ++i)
            {
                Assert.AreEqual(names.Length, lists[i].Length, "Wrong number of field infos!");
                for (int j = 0; j < lists[i].Length; ++j)
                {
                    FieldInfo f = lists[i][j];
                    Assert.IsNotNull(f, "Field is null for item '{0}.{1}'!", i, j);
                    Assert.IsNotNull(names.FirstOrDefault(l => l == f.Name), "Wrong name!");
                }
            }


            Assert.AreEqual(1, FieldInfoCache.FieldInfoArrayCount(), "Wrong array field count after test!");
        }


        private async Task GetFieldInfoAsync(int i, Type t, string fieldName)
        {
            await Task.Delay(TimeSpan.FromMilliseconds(Total + 10 - i)).ConfigureAwait(false);
            FieldInfo fieldInfo = FieldInfoCache.GetFieldInfo(t, fieldName);
            Assert.IsNotNull(fieldInfo, "Could not get field info!");
            _fieldInfoQueue.Enqueue(fieldInfo);
        }


        private async Task GetFieldInfosAsync(int i, Type t)
        {
            await Task.Delay(TimeSpan.FromMilliseconds(Total + 10 - i)).ConfigureAwait(false);
            var fieldInfos = FieldInfoCache.GetFieldInfos(t);
            Assert.IsNotNull(fieldInfos, "Could not get field info!");
            Assert.AreNotEqual(0, fieldInfos.Length, "Could not get enough field info!");
            _fieldInfosQueue.Enqueue(fieldInfos);
        }

/*
        [Test]
        public void NoReadOnlyFlagTest()
        {
            var fieldInfos = FieldInfoCache.GetFieldInfos(typeof(ReadonlyFieldTest));
            Assert.AreEqual(1, fieldInfos.Length);
            fieldInfos = FieldInfoCache.GetFieldInfos(typeof(ReadonlyFieldTest), FieldFlags.NoReadOnly);
            Assert.AreEqual(0, fieldInfos.Length);     }*/
    }
}