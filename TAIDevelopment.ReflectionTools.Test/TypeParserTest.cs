﻿using System;
using System.Diagnostics;
using NUnit.Framework;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class TypeParserTest
    {
        [Test]
        public void TestString()
        {
            {
                const string str = "\"asdf\"";
                object o = TypeParser.Parse<string>(str);
                string s = (string)o;
                Assert.AreEqual("asdf", s);
            }
            {
                const string str = "qwer";
                object o = TypeParser.Parse<string>(str);
                string s = (string)o;
                Assert.AreEqual("qwer", s);
            }
            {
                const string str = "\"\"";
                object o = TypeParser.Parse<string>(str);
                string s = (string)o;
                Assert.AreEqual("", s);
            }
            {
                const string str = "null";
                object o = TypeParser.Parse<string>(str);
                string s = (string)o;
                Assert.AreEqual(null, s);
            }
        }

        [Test]
        public void TestLong()
        {
            const string str = "9223372036854775807";
            object o = TypeParser.Parse<long>(str);
            long l = (long)o;
            Assert.AreEqual(9223372036854775807L, l);
        }

        [Test]
        public void TestInt()
        {
            const string str = "-2147483648";
            object o = TypeParser.Parse<int>(str);
            int i = (int)o;
            Assert.AreEqual(-2147483648, i);
        }

        [Test]
        public void TestShort()
        {
            const string str = "32767";
            object o = TypeParser.Parse<short>(str);
            short s = (short)o;
            Assert.AreEqual(32767, s);
        }

        [Test]
        public void TestByte()
        {
            const string str = "255";
            object o = TypeParser.Parse<byte>(str);
            byte b = (byte)o;
            Assert.AreEqual(255, b);
        }
        [Test]
        public void TestULong()
        {
            const string str = "18446744073709551615";
            object o = TypeParser.Parse<ulong>(str);
            ulong l = (ulong)o;
            Assert.AreEqual(18446744073709551615UL, l);
        }

        [Test]
        public void TestUInt()
        {
            const string str = "4294967295";
            object o = TypeParser.Parse<uint>(str);
            uint i = (uint)o;
            Assert.AreEqual(4294967295, i);
        }

        [Test]
        public void TestUShort()
        {
            const string str = "65535";
            object o = TypeParser.Parse<ushort>(str);
            ushort s = (ushort)o;
            Assert.AreEqual(65535, s);
        }

        [Test]
        public void TestBool()
        {
            const string str = "true";
            object o = TypeParser.Parse<bool>(str);
            bool b = (bool)o;
            Assert.AreEqual(true, b);
        }

        [Test]
        public void TestNullableBool()
        {
            {
                const string str = "true";
                object o = TypeParser.Parse<bool?>(str);
                bool? b = (bool?)o;
                Assert.AreEqual(true, b);
            }
            {
                const string str = "null";
                object o = TypeParser.Parse<bool?>(str);
                bool? b = (bool?)o;
                Assert.AreEqual(null, b);
            }
        }

        [Test]
        public void TestChar()
        {
            const string str = "Z";
            object o = TypeParser.Parse<char>(str);
            char c = (char)o;
            Assert.AreEqual('Z', c);
        }

        [Test]
        public void TestNullableChar()
        {
            {
                const string str = "Z";
                object o = TypeParser.Parse<char?>(str);
                char? c = (char?)o;
                Assert.AreEqual('Z', c);
            }
            {
                const string str = "null";
                object o = TypeParser.Parse<char?>(str);
                char? c = (char?)o;
                Assert.AreEqual(null, c);
            }
        }


        [Test]
        public void TestSystemType()
        {
#if !(NETCOREAPP)
            const string str = "\"System.Diagnostics.Stopwatch, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"";
#else
            const string str = "\"System.Diagnostics.Stopwatch, System.Runtime.Extensions\"";
#endif

            Type t = TypeParser.Parse<Type>(str);
            Assert.AreEqual(typeof(Stopwatch), t);
        }

    }
}
