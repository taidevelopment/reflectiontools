﻿using System;
using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class FieldReaderTest
    {
        [Test]
        public void GetValueTest()
        {
            Assert.Throws<ArgumentNullException>(() => { new FieldReader<FieldTestClass, string>(null, "asdf"); },
                "There should be an ArgumentNullException because of the first argument which is null!");
            Assert.Throws<ArgumentNullException>(() => { new FieldReader<FieldTestClass, string>(typeof(FieldTestClass), (string)null); }, 
                "There should be an ArgumentNullException because of the second argument which is null!");

            {
                FieldReader<FieldTestClass, int> fieldReader = new FieldReader<FieldTestClass, int>(typeof(FieldTestClass), "PublicIntegerValue");

                FieldTestClass f = new FieldTestClass();
                f.PublicIntegerValue = 5;

                {
                    int value = fieldReader.GetValue(f);
                    Assert.AreEqual(f.PublicIntegerValue, value, "Wrong value!");
                    object valueObj = fieldReader.GetUntypedValue(f);
                    Assert.AreEqual(f.PublicIntegerValue, valueObj, "Wrong object value!");
                }

                {
                    f.PublicIntegerValue = 6;
                    int valueNew = fieldReader.GetValue(f);
                    Assert.AreEqual(f.PublicIntegerValue, valueNew, "Wrong new value!");
                    object valueObjNew = fieldReader.GetUntypedValue(f);
                    Assert.AreEqual(f.PublicIntegerValue, valueObjNew, "Wrong new object value!");
                }

            }

        }


        [Explicit("TODO: This test fails because the binding flags in the FieldInfoCache don't include 'static'. Maybe it is on purpose?")]
        [Test]
        public void ReadStaticValueTest()
        {
            FieldReader<WithStaticField, string> fieldReader = new FieldReader<WithStaticField, string>(typeof(WithStaticField), "Name");

            WithStaticField f = new WithStaticField();
            {
                string v = fieldReader.GetValue(f);
                Assert.AreEqual(v, WithStaticField.Name, "Wrong value!");
                object o = fieldReader.GetUntypedValue(f);
                Assert.AreEqual(o, (object)WithStaticField.Name, "Wrong object value!");
            }
        }

    }
}
