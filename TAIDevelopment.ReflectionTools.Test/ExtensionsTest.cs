using NUnit.Framework;
using TAIDevelopment.ReflectionTools.Extensions;
using TAIDevelopment.ReflectionTools.Test.TestCases;

namespace TAIDevelopment.ReflectionTools.Test
{
    [TestFixture]
    public class ExtensionsTest
    {
        
        [Test]
        public void ExtensionMethodTest()
        {
            FieldTestClass f = new FieldTestClass();
            f.PublicIntegerValue = 5;
            Assert.AreEqual(5, f.GetValue<int>(nameof(FieldTestClass.PublicIntegerValue)));
            
            f.SetValue(nameof(FieldTestClass.PublicIntegerValue), 2);
            Assert.AreEqual(2, f.PublicIntegerValue );
        }
    }
}