# TAIDevelopment.ReflectionTools

Efficient CLR reflection tools.


Usage example:

```
using TAIDevelopment.ReflectionTools.Extensions;


var obj = new MyExampleClass();
obj.SetValue("MyField", 2);

var value = obj.GetValue<int>("MyField")

```


Issues are tracked on the gitlab project: https://gitlab.com/taidevelopment/reflectiontools/
